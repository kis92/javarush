package com.javarush.test.level10.lesson11.home05;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

/* Количество букв
Ввести с клавиатуры 10 строчек и подсчитать в них количество различных букв (для 33 букв алфавита).  Вывести результат на экран.
Пример вывода:
а 5
б 8
в 3
г 7
…
я 9
*/

public class Solution
{
    public static int countNum = 0;
    public static void main(String[] args)  throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        //алфавит
        ArrayList<Character> alphabet = new ArrayList<Character>();
        for(int i=0;i<32;i++)
        {
            alphabet.add( (char) ('а'+i));
        }
        alphabet.add(6,'ё');

        //ввод строк
        ArrayList<String> list = new ArrayList<String>();
        for(int i=0;i<10;i++)
        {
            String s = reader.readLine();
            list.add( s.toLowerCase());
        }

        Map<Character, Integer> map = new LinkedHashMap<Character, Integer>();
        for (int i = 0; i < alphabet.size(); i++){
            map.put(alphabet.get(i), count(list, alphabet));
        }
        for (Map.Entry<Character, Integer> x : map.entrySet()){
            Character key = x.getKey();
            int Value = x.getValue();
            System.out.println(key + " " + Value);


        }
    }
    public static Integer count(ArrayList<String> list, ArrayList<Character> alphabet){
        char[] a;
        int innerCount = 0;
        for (int i = 0; i < list.size(); i++){
            for (int j = 0; j < list.get(i).length(); j++){
                if (alphabet.get(countNum).equals(list.get(i).charAt(j))){
                    innerCount++;
                }
            }

        }
        countNum++;
        return innerCount;
    }
}

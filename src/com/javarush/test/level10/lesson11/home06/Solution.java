package com.javarush.test.level10.lesson11.home06;

/* Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*/

import java.util.Date;

public class Solution
{
    public static void main(String[] args)
    {

    }

    public static class Human
    {
        //напишите тут ваши переменные и конст,lastName
        String firstName, lastName, address;
        Date birthday;
        int age;
        boolean sex;

        public Human(String firstName, String lastName, String address, Date birthday, int age, boolean sex)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.address = address;
            this.birthday = birthday;
            this.age = age;
            this.sex = sex;
        }
        public Human(String firstName, String lastName, String address)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.address = address;
        }
        public Human(String firstName, String lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public Human(String firstName, String lastName, int age, boolean sex)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;
            this.sex = sex;
        }

        public Human()
        {
        }

        public Human(String firstName)
        {
            this.firstName = firstName;
        }

        public Human(Date birthday)
        {
            this.birthday = birthday;
        }

        public Human(boolean sex)
        {
            this.sex = sex;
        }

        public Human(int age, boolean sex)
        {
            this.age = age;
            this.sex = sex;
        }

        public Human(Date birthday, int age, boolean sex)
        {
            this.birthday = birthday;
            this.age = age;
            this.sex = sex;
        }
    }
}

package com.javarush.test.level06.lesson11.bonus02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* Нужно добавить в программу новую функциональность
Задача: У каждой кошки есть имя и кошка-мама.
Создать класс, который бы описывал данную ситуацию.
Создать два объекта: кошку-дочь и кошку-маму. Вывести их на экран.

Новая задача: У каждой кошки есть имя, кошка-папа и кошка-мама.
Изменить класс Cat так, чтобы он мог описать данную ситуацию.
Создать 6 объектов: маму, папу, сына, дочь, бабушку(мамина мама) и дедушку(папин папа).
Вывести их всех на экран в порядке: дедушка, бабушка, папа, мама, сын, дочь.

Пример ввода:
дедушка Вася
бабушка Мурка
папа Котофей
мама Василиса
сын Мурчик
дочь Пушинка

Пример вывода:
Cat name is дедушка Вася, no mother, no father
Cat name is бабушка Мурка, no mother, no father
Cat name is папа Котофей, no mother, father is дедушка Вася
Cat name is мама Василиса, mother is бабушка Мурка, no father
Cat name is сын Мурчик, mother is мама Василиса, father is папа Котофей
Cat name is дочь Пушинка, mother is мама Василиса, father is папа Котофей
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String grandPaName = reader.readLine();
        Cat catGrandPa = new Cat(grandPaName);
        String grandMaName = reader.readLine();
        Cat catGrandMa = new Cat(grandMaName);
        String paName = reader.readLine();
        Cat catPa = new Cat(paName, catGrandPa, null);
        String maName = reader.readLine();
        Cat catMa = new Cat(maName, null, catGrandMa);
        String sanName = reader.readLine();
        Cat catSan = new Cat(sanName, catPa, catMa);
        String douName = reader.readLine();
        Cat catDou = new Cat(douName, catPa, catMa);

        System.out.println(catGrandPa);
        System.out.println(catGrandMa);
        System.out.println(catPa);
        System.out.println(catMa);
        System.out.println(catSan);
        System.out.println(catDou);

    }

    public static class Cat
    {
        private String name;
        private Cat parentPa;
        private Cat parentMa;

        public Cat(String name)
        {
            this.name = name;
        }

        public Cat(String name, Cat parentPa, Cat parentMa)
        {
            this.name = name;
            this.parentPa = parentPa;
            this.parentMa = parentMa;
        }

        @Override
        public String toString()
        {
            if ((parentMa == null)&&(parentPa == null))
                return "Cat name is " + name + ", no mother, no father";
            else if ((parentMa != null)&&(parentPa == null))
                return "Cat name is " + name + ", mother is " + parentMa.name + ", no father";
            else if ((parentMa == null)&&(parentPa != null))
                return "Cat name is " + name + ", no mother, father is " + parentPa.name;
            else
                return "Cat name is " + name + ", mother is " + parentMa.name +", father is " + parentPa.name;
        }
    }

}

package com.javarush.test.level05.lesson12.bonus02;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* Нужно добавить в программу новую функциональность
Задача: Программа вводит два числа с клавиатуры и выводит минимальное из них на экран.
Новая задача: Программа вводит пять чисел с клавиатуры и выводит минимальное из них на экран.
*/

public class Solution
{

    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());

        int minimum = min(a, b);

        int a1 = Integer.parseInt(reader.readLine());
        int b1 = Integer.parseInt(reader.readLine());

        int minimum1 = min(a1, b1);

        int q = Integer.parseInt(reader.readLine());

        int minimum2 = min(minimum, minimum1);
        int minimumMIN = min(minimum2, q);
        System.out.println("Minimum = " + minimumMIN);
    }


    public static int min(int a, int b)
    {
        return a < b ? a : b;
    }

}

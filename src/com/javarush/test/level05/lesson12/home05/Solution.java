package com.javarush.test.level05.lesson12.home05;

/* Вводить с клавиатуры числа и считать их сумму
Вводить с клавиатуры числа и считать их сумму,
пока пользователь не введёт слово «сумма».
Вывести на экран полученную сумму.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //Напишите тут ваш код
        int summ = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for(;;){
            try{
                int input = Integer.parseInt(reader.readLine());
                summ = summ + input;
            }catch (Exception ex){
                System.out.print(summ);
                break;
            }
        }
    }
}

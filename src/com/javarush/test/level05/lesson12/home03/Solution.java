package com.javarush.test.level05.lesson12.home03;

/* Создай классы Dog, Cat, Mouse
Создай классы Dog, Cat, Mouse. Добавь по три поля в каждый класс, на твой выбор. Создай объекты для героев мультика Том и Джерри. Так много, как только вспомнишь.
Пример:
Mouse jerryMouse = new Mouse(“Jerry”, 12 , 5), где 12 - высота в см, 5 - длина хвоста в см.
*/

public class Solution
{
    public static void main(String[] args)
    {
        Mouse jerryMouse = new Mouse("Jerry", 12 , 5);
        Dog spike = new Dog("Spike", 2, 4);
        Dog tike = new Dog("Tike", 2, 4);
        Dog butch = new Dog("Butch", 2, 4);
        Cat tuts = new Cat("Tutz", 2, 4);
        Cat tydlz = new Cat("Tydlz", 3, 5);
        Cat tom = new Cat("Tom", 5, 4);

        //Напишите тут ваш код
    }

    public static class Mouse
    {
        String name;
        int height;
        int tail;

        public Mouse(String name, int height, int tail)
        {
            this.name = name;
            this.height = height;
            this.tail = tail;
        }
    }

    //Напишите тут ваши классы

    public static class Cat{
        String name;
        int age;
        int weight;

        public Cat(String name, int age, int weight)
        {
            this.name = name;
            this.age = age;
            this.weight = weight;
        }
    }

    public static class Dog{
        String name;
        int age;
        int strange;

        public Dog(String name, int age, int strange)
        {
            this.name = name;
            this.age = age;
            this.strange = strange;
        }
    }

}

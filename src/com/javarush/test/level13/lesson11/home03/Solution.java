package com.javarush.test.level13.lesson11.home03;

/* Чтение файла
1. Считать с консоли имя файла.
2. Вывести в консоль(на экран) содержимое файла.
3. Не забыть закрыть файл и поток.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        //add your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        File file = new File(reader.readLine());

        FileReader fileReader = new FileReader(file);
        BufferedReader reader2 = new BufferedReader(fileReader);
        while(reader2.ready()){
            System.out.println(reader2.readLine());
        }
        fileReader.close();

    }
}

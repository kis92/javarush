package com.javarush.test.level04.lesson06.task02;

/* Максимум четырех чисел
Ввести с клавиатуры четыре числа, и вывести максимальное из них.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Solution
{
//    int max(int first, int second)
//    {
//        int maximum = 0;
//        if (first == second)
//        {
//            maximum = first;
//        } else{
//            if (first < second)
//            {
//                maximum = first;
//            } else
//            {
//                maximum = second;
//            }
//        }
//        return maximum;
//    }

    public static void  main(String[] args) throws Exception
    {
        //Напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] arr = new int[4];
        for(int i = 0; i < 4; i++){
            arr[i] = Integer.parseInt(reader.readLine());
        }
        Arrays.sort(arr);
        System.out.print(arr[arr.length-1]);


//        int first = Integer.parseInt(reader.readLine());
//        int second = Integer.parseInt(reader.readLine());
//        int three = Integer.parseInt(reader.readLine());
//        int four = Integer.parseInt(reader.readLine());
//        int fiSec = max(first, second);
//        int thFo = max(three, four);
//
//        int fiTh = max(first, three);
//        int fiFo = max(first, four);

    }
}

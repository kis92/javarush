package com.javarush.test.level04.lesson16.home04;

/* Меня зовут 'Вася'...
Ввести с клавиатуры строку name.
Ввести с клавиатуры дату рождения (три числа): y, m, d.
Вывести на экран текст:
«Меня зовут name
Я родился d.m.y»
Пример:
Меня зовут Вася
Я родился 15.2.1988
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args)   throws Exception
    {
        //Напишите тут ваш код
        BufferedReader reader = new BufferedReader( new InputStreamReader( System.in));
        String name = reader.readLine();
//        String date = reader.readLine();
//        String zap = ""+',';
//        String date2 = date.replaceAll(zap,".");

//        int year = Integer.parseInt(date.substring(0,4));
//        int month = Integer.parseInt(date.substring(5,7));
//        int day = Integer.parseInt(date.substring(8));
        int year = Integer.parseInt(reader.readLine());
        int month = Integer.parseInt(reader.readLine());
        int day = Integer.parseInt(reader.readLine());

        System.out.println("Меня зовут " + name + " ");
        System.out.println("Я родился " + day + "." + month + "." + year);
//        System.out.println("Я родился " + date2);
    }
}


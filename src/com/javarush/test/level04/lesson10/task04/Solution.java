package com.javarush.test.level04.lesson10.task04;

/* S-квадрат
Вывести на экран квадрат из 10х10 букв S используя цикл while.
Буквы в одной строке не разделять.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //Напишите тут ваш код
        int countStolbcov = 10;
        int countStrok = 10;

        while (countStrok > 0){
            while (countStolbcov > 0){
                System.out.print("S");
                countStolbcov--;
            }
            System.out.println();
            countStolbcov = 10;
            countStrok--;
        }
    }
}

package com.javarush.test.level07.lesson09.task01;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* Три массива
1. Введи с клавиатуры 20 чисел, сохрани их в список и рассортируй по трём другим спискам:
Число делится на 3 (x%3==0), делится на 2 (x%2==0) и все остальные.
Числа, которые делятся на 3 и на 2 одновременно, например 6, попадают в оба списка.
2. Метод printList должен выводить на экран все элементы списка  с новой строки.
3. Используя метод printList выведи эти три списка на экран.
Сначала тот, который для x%3, потом тот, который для x%2, потом последний.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //add your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Integer> globalMod3 = new ArrayList<Integer>();
        ArrayList<Integer> globalMod2 = new ArrayList<Integer>();
        ArrayList<Integer> globalAny = new ArrayList<Integer>();
        for (int i = 0; i < 20; i++){
            int x = Integer.parseInt(reader.readLine());
            if ((x%3 == 0) && (x%2 == 0)){
                globalMod3.add(x);
                globalMod2.add(x);
            } else if (x%3 == 0){
                globalMod3.add(x);
            } else if (x%2 == 0){
                globalMod2.add(x);
            } else {
                globalAny.add(x);
            }
        }
        printList(globalMod3);
        printList(globalMod2);
        printList(globalAny);
    }

    public static void printList(List<Integer> list) {
        //add your code here
        for (Integer aList : list)
        {
            System.out.println(aList);
        }
        System.out.println();
    }
}

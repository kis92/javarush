package com.javarush.test.level07.lesson09.task03;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

/* Слово «именно»
1. Создай список из слов «мама», «мыла», «раму».
2. После каждого слова вставь в список строку, содержащую слово «именно».
3. Используя цикл for вывести результат на экран, каждый элемент списка с новой строки.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //Напишите тут ваш код
        ArrayList<String> strings = new ArrayList<String>();
        String[] strs = {"мама", "мыла", "раму"};
        Collections.addAll(strings,strs);
        for (int i = 0; i < 6; i++)
        {
            if (i%2 != 0){
                strings.add(i, "именно");
            }
        }
//        strings.add(1, "именно");
//        strings.add(3, "именно");
//        strings.add(5,"именно");
        for (String aList : strings)
        {
            System.out.println(aList);
        }

    }
}

package com.javarush.test.level07.lesson12.home03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/* Максимальное и минимальное числа в массиве
Создать массив на 20 чисел. Заполнить его числами с клавиатуры. Найти максимальное и минимальное числа в массиве.
Вывести на экран максимальное и минимальное числа через пробел.
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int  maximum;
        int  minimum;

        //Напишите тут ваш код
        TreeSet<Integer> integers = new TreeSet<Integer>();
        for (int i = 0; i < 20; i++)
        {
            integers.add(Integer.parseInt(reader.readLine()));
        }

        maximum = integers.last();
        minimum = integers.first();
        System.out.println(maximum);
        System.out.println(minimum);
    }
}

package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human).
Создай объекты и заполни их так, чтобы получилось:
Два дедушки, две бабушки, отец, мать, трое детей.
Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human,
то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/

public class Solution
{
    public static void main(String[] args)
    {
        //Написать тут ваш код
        Human grandPaOne = new Human("grandPaOne", true, 60, null,null);
        System.out.println(grandPaOne);
        Human grandPaTwo = new Human("grandPaTwo", true, 66, null,null);
        System.out.println(grandPaTwo);

        Human grandMaOne = new Human("grandMaOne", false, 59, null,null);
        System.out.println(grandMaOne);
        Human grandMaTwo = new Human("grandMaTwo", false, 65, null,null);
        System.out.println(grandMaTwo);

        Human father = new Human("father", true, 30, grandPaOne, grandMaOne);
        System.out.println(father);
        Human mother = new Human("mother", false, 29, grandPaTwo, grandMaTwo);
        System.out.println(mother);

        Human sunOne = new Human("sunOne", true, 10, father, mother);
        System.out.println(sunOne);
        Human sunTwo = new Human("sunTwo", true, 10, father, mother);
        System.out.println(sunTwo);
        Human daugher = new Human("daugher", false, 10, father, mother);
        System.out.println(daugher);


    }

    public static class Human
    {
        //Написать тут ваш код
        String name;
        boolean sex;
        int age;
        Human father;
        Human mother;

        public Human(String name, boolean sex, int age, Human father, Human mother)
        {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }

}

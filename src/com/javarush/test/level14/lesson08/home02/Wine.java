package com.javarush.test.level14.lesson08.home02;

/**
 * Created by Kochubey-I on 03.04.15.
 */
//Создать класс Wine, который наследуется от Drink, с реализованным методом public String getHolidayName(), который возвращает строку "День рождения"
public class Wine extends Drink
{
    public String getHolidayName(){
        return "День рождения";
    }
}

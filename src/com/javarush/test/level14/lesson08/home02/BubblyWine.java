package com.javarush.test.level14.lesson08.home02;

/**
 * Created by Kochubey-I on 03.04.15.
 */
//Создать класс BubblyWine, который наследуется от Wine, с реализованным методом public String getHolidayName(), который возвращает строку "Новый год"
public class BubblyWine extends Wine
{
    public String getHolidayName(){
        return "Новый год";
    }
}

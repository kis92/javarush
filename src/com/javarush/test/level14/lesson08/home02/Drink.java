package com.javarush.test.level14.lesson08.home02;

/**
 * Created by Kochubey-I on 03.04.15.
 */
abstract public class Drink
{
    public void taste()
    {
        System.out.println("Вкусно");
    }
}

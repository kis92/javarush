package com.javarush.test.level14.lesson08.bonus01;

import java.util.ArrayList;
import java.util.List;

/* Нашествие эксепшенов
Заполни массив exceptions 10 различными эксепшенами.
Первое исключение уже реализовано в методе initExceptions.
*/

public class Solution
{
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args)
    {
        initExceptions();

        for (Exception exception : exceptions)
        {
            System.out.println(exception);
        }
    }

    private static void initExceptions()
    {   //it's first exception
        try
        {
            float i = 1 / 0;

        }
        catch (Exception e)
        {
            exceptions.add(e);
        }

        //Add your code here

        try
        {
            String str = null;
            str.toString();
        }
        catch (Exception e)
        {
            exceptions.add(e);
        }


        try
        {
            Exception qwe = new RuntimeException("ex");
            exceptions.add(qwe);
        }
        catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            Exception qwe = new Exception("ex");
            exceptions.add(qwe);
        }
        catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {

            Exception qwe = new ClassCastException("qwe");
            exceptions.add(qwe);
        }
        catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            Exception qwe = new ClassNotFoundException("rty"); new ClassCastException("qwe");
            exceptions.add(qwe);
        }
        catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            Exception qwe =new NegativeArraySizeException("sdas"); new ClassCastException("qwe");
            exceptions.add(qwe);
        }
        catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            Exception qwe =  new ArrayStoreException("asd");new ClassCastException("qwe");
            exceptions.add(qwe);
        }
        catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            Exception qwe = new SecurityException("asdas");new ClassCastException("qwe");
            exceptions.add(qwe);
        }
        catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            Exception qwe =new NumberFormatException("asdsdasdad"); new ClassCastException("qwe");
            exceptions.add(qwe);
        }
        catch (Exception e)
        {
            exceptions.add(e);
        }
    }
}

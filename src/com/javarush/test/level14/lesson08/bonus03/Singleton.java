package com.javarush.test.level14.lesson08.bonus03;

/**
 * Created by Kochubey-I on 03.04.15.
 */
public class Singleton
{
    private static final Singleton singl = new Singleton();

    private Singleton()
    {
    }

    static Singleton getInstance(){
        return singl;
    }
}

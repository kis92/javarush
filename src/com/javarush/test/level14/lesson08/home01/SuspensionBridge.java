package com.javarush.test.level14.lesson08.home01;

/**
 * Created by Kochubey-I on 03.04.15.
 */
public class SuspensionBridge implements Bridge
{
    @Override
    public int getCarsCount()
    {
        return 20;
    }
}

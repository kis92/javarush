package com.javarush.test.level14.lesson08.bonus02;

/* НОД
Наибольший общий делитель (НОД).
Ввести с клавиатуры 2 целых положительных числа.
Вывести в консоль наибольший общий делитель.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int f = Integer.parseInt(reader.readLine());

        Set<Integer> aSet = getSet(a);
        Set<Integer> fSet = getSet(f);
        if(a>f){
            aSet.retainAll(fSet);
            Integer[] arr = aSet.toArray(new Integer[aSet.size()]);
            System.out.println(arr[arr.length-1]);
        }else{
            fSet.retainAll(aSet);
            Integer[] arr = fSet.toArray(new Integer[fSet.size()]);
            System.out.println(arr[arr.length-1]);
        }

    }

    private static Set<Integer> getSet(int num){
        Set<Integer> set = new HashSet<Integer>();
        for(int i = 1; i <= num; i++){
            int deliver = num / i;
            set.add(deliver);
        }
        return set;
    }
}

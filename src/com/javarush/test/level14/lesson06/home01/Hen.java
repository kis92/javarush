package com.javarush.test.level14.lesson06.home01;

/**
 * Created by Kochubey-I on 01.04.15.
 */
public abstract class Hen
{
    abstract int getCountOfEggsPerMonth();
    String getDescription(){
        return "Я курица.";
    }
}

package com.javarush.test.level14.lesson06.home01;

/**
 * Created by Kochubey-I on 01.04.15.
 */
public class MoldovanHen extends Hen
{


    public MoldovanHen()
    {

    }

    @Override
    int getCountOfEggsPerMonth()
    {
        return 2;
    }

    @Override
    String getDescription()
    {
        return super.getDescription() + " Моя страна - "+Country.MOLDOVA+". Я несу "+ getCountOfEggsPerMonth()+" яиц в месяц.";
    }
}

package com.javarush.test.level14.lesson06.home01;

/**
 * Created by Kochubey-I on 01.04.15.
 */
public class UkrainianHen extends Hen
{


    public UkrainianHen()
    {

    }

    @Override
    int getCountOfEggsPerMonth()
    {
        return 3;
    }

    @Override
    String getDescription()
    {
        return super.getDescription() + " Моя страна - "+Country.UKRAINE+". Я несу "+ getCountOfEggsPerMonth()+" яиц в месяц.";
    }
}

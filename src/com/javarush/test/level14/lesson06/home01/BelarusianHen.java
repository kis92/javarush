package com.javarush.test.level14.lesson06.home01;

/**
 * Created by Kochubey-I on 01.04.15.
 */
public class BelarusianHen extends Hen
{
    public BelarusianHen()
    {
    }

    @Override
    int getCountOfEggsPerMonth()
    {
        return 1;
    }

    @Override
    String getDescription()
    {
        return super.getDescription() + " Моя страна - "+Country.BELARUS+". Я несу "+ getCountOfEggsPerMonth()+" яиц в месяц.";
    }
}

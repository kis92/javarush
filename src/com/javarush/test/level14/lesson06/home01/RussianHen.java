package com.javarush.test.level14.lesson06.home01;

/**
 * Created by Kochubey-I on 01.04.15.
 */
public class RussianHen extends Hen
{
    public RussianHen()
    {
    }

    @Override
    int getCountOfEggsPerMonth()
    {
        return 4;
    }

    @Override
    String getDescription()
    {
        return super.getDescription() + " Моя страна - "+Country.RUSSIA+". Я несу "+ getCountOfEggsPerMonth()+" яиц в месяц.";
    }

}

package com.javarush.test.level09.lesson11.bonus03;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

/* Задача по алгоритмам
Задача: Пользователь вводит с клавиатуры список слов (и чисел). Слова вывести в возрастающем порядке, числа - в убывающем.
Пример ввода:
Вишня
1
Боб
3
Яблоко
2
0
Арбуз
Пример вывода:
Арбуз
3
Боб
2
Вишня
1
0
Яблоко
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<String>();
        while (true)
        {
            String s = reader.readLine();
            if (s.isEmpty()) break;
            list.add(s);
        }

        String[] array = list.toArray(new String[list.size()]);
        sort(array);

        for (String x : array)
        {
            System.out.println(x);
        }
    }

    public static void sort(String[] array)
    {
        //Напишите тут ваш код
        ArrayList<Integer> integers = new ArrayList<Integer>();
        int[] posInt = new int[array.length];
        ArrayList<String> strings = new ArrayList<String>();

        for (int i = 0; i < array.length; i++)
        {
            String str = array[i];
            if (isNumber(str))
            {
                integers.add(Integer.parseInt(str));
                posInt[i] = i;
            } else
            {
                strings.add(str);
                posInt[i] = -1;
            }
        }
        //sort integers
        Object[] arrInt = integers.toArray();
        Arrays.sort(arrInt);

        for (int i = 0; i < arrInt.length/2; i++)
        {
            Object temp = arrInt[i];
            arrInt[i] = arrInt[arrInt.length-i-1];
            arrInt[arrInt.length-i-1] = temp;
        }
        //sort strings
        Object[] arrString = strings.toArray();
        Arrays.sort(arrString);
        //output
        for (int i = 0, j=0, q = 0; i < array.length; i++)
        {
            if (i == posInt[i])
            {
                array[i] = String.valueOf(arrInt[j]);
                j++;
            } else
            {
                array[i] = String.valueOf(arrString[q]);
                q++;
            }
        }


    }

    //Метод для сравнения строк: 'а' больше чем 'b'
    public static boolean isGreaterThen(String a, String b)
    {
        return a.compareTo(b) > 0;
    }


    //строка - это на самом деле число?
    public static boolean isNumber(String s)
    {
        if (s.length() == 0) return false;

        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++)
        {
            char c = chars[i];
            if ((i != 0 && c == '-') //есть '-' внутри строки
                    || (!Character.isDigit(c) && c != '-')) // не цифра и не начинается с '-'
            {
                return false;
            }
        }
        return true;
    }
}

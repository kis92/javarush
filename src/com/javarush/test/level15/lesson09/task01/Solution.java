package com.javarush.test.level15.lesson09.task01;

import java.util.HashMap;
import java.util.Map;

/* Статики 1
В статическом блоке инициализировать labels 5 различными парами.
*/

public class Solution {
    public static Map<Double, String> labels = new HashMap<Double, String>();

    public static void main(String[] args) {
        System.out.println(labels);
    }

    static{
        labels.put(1.0,"q");
        labels.put(1.1,"qa");
        labels.put(1.2,"qz");
        labels.put(1.3,"qx");
        labels.put(1.4,"qc");
    }
}

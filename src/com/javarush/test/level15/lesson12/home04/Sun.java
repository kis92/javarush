package com.javarush.test.level15.lesson12.home04;

/**
 * Created || Updated by Kochubey-I on 27.04.2015 in 11:22.
 */
public class Sun implements Planet
{
    private static Sun instance;

    private Sun(){  }

    public static Sun getInstance()
    {
        if (instance == null)
            instance = new Sun();

        return instance;
    }
}

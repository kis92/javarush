package com.javarush.test.level15.lesson12.home04;

/**
 * Created || Updated by Kochubey-I on 27.04.2015 in 11:22.
 */
public class Earth implements Planet
{
    private static Earth uniqueInstance;

    private Earth()
    {
    }

    public static Earth getInstance()
    {
        if(uniqueInstance == null){
            uniqueInstance = new Earth();
        }
        return uniqueInstance;
    }
}

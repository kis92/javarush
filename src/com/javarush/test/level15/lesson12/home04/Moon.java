package com.javarush.test.level15.lesson12.home04;

/**
 * Created || Updated by Kochubey-I on 27.04.2015 in 11:22.
 */
public class Moon implements Planet
{
    private static Moon uniqueInstance;

    private Moon()
    {
    }

    public static Moon getInstance()
    {
        if(uniqueInstance == null){
            uniqueInstance = new Moon();
        }
        return uniqueInstance;
    }
}

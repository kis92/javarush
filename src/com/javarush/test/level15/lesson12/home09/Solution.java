package com.javarush.test.level15.lesson12.home09;

/* Парсер реквестов
Считать с консоли URl ссылку.
Вывести на экран через пробел список всех параметров (Параметры идут после ? и разделяются &, например, lvl=15).
URL содержит минимум 1 параметр.
Если присутствует параметр obj, то передать его значение в нужный метод alert.
alert(double value) - для чисел (дробные числа разделяются точкой)
alert(String value) - для строк

Пример 1
Ввод:
http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo
Вывод:
lvl view name

Пример 2
Ввод:
http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo
Вывод:
obj name
double 3.14
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        //add your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String url = reader.readLine();
        int start = url.indexOf("?") + 1;
        String parameter = url.substring(start);

        String[] params = parameter.split("&");
        Map<String, String> map = new LinkedHashMap<String, String>();
        for(String str : params){
            String key;
            String val = "";
            if(str.contains("=")){
                int strt = str.indexOf("=");
                key = str.substring(0,strt);
                val = str.substring(strt+1);
                map.put(key, val);
            }else{
                key = str;
                map.put(key, val);
            }
        }

        for(String keyMap : map.keySet()){
            System.out.print(keyMap + " ");
        }
        System.out.println();
        for(String keyMap : map.keySet()){
            if(keyMap.equals("obj")){
                String val = map.get("obj");
                try{
                    double d = Double.parseDouble(val);
                    alert(d);
                }catch(Exception e ) {
                    alert(val);
                }
            }
        }
    }

    public static void alert(double value) {
        System.out.println("double " + value);
    }

    public static void alert(String value) {
        System.out.println("String " + value);
    }
}

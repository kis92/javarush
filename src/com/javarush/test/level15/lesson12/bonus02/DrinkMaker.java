package com.javarush.test.level15.lesson12.bonus02;

/**
 * Created || Updated by Kochubey-I on 30.04.2015 in 16:10.
 */

public abstract class DrinkMaker
{
    abstract void getRightCup();
    abstract void putIngredient();
    abstract void pour();

    void makeDrink(){
        getRightCup();
        putIngredient();
        pour();
    }
}

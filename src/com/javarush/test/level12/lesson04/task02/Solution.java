package com.javarush.test.level12.lesson04.task02;

/* print(int) и print(Integer)
Написать два метода: print(int) и print(Integer).
Написать такой код в методе main, чтобы вызвались они оба.
*/

public class Solution
{
    public static void main(String[] args)
    {
        int i = 1;
        print(i);

        Integer integer = 1;
        print(integer);
    }

    //Напишите тут ваши методы
    static void print(int input){

    }

    static void print(Integer input){

    }
}

package com.javarush.test.level08.lesson06.task01;

import java.util.*;

/* Создать два списка LinkedList и ArrayList
Нужно создать два списка – LinkedList и ArrayList.
*/

public class Solution
{
    public static Object createArrayList()
    {
        //Напишите тут ваш код
        List<Object> objects = new ArrayList<Object>();
        return objects;

    }

    public static Object createLinkedList()
    {
        //Напишите тут ваш код
        List<Object> objects = new LinkedList<Object>();
        return objects;
    }
}

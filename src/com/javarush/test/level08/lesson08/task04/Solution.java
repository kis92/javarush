package com.javarush.test.level08.lesson08.task04;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Удалить всех людей, родившихся летом
Создать словарь (Map<String, Date>) и занести в него десять записей по принципу: «фамилия» - «дата рождения».
Удалить из словаря всех людей, родившихся летом.
*/

public class Solution
{
    public static HashMap<String, Date> createMap()
    {
        HashMap<String, Date> map = new HashMap<String, Date>();
        map.put("Сталлоне0", new Date("JUNE 1 1980"));
        map.put("Сталлоне1", new Date("JULY 1 1980"));
        map.put("Сталлоне2", new Date("AUGUST 1 1982"));
        map.put("Сталлоне3", new Date("OCTOBER 1 1980"));
        map.put("Сталлоне4", new Date("DECEMBER 1 1980"));
        map.put("Сталлоне5", new Date("JUNE 1 1983"));
        map.put("Сталлоне6", new Date("JUNE 1 1984"));
        map.put("Сталлоне7", new Date("JUNE 1 1985"));
        map.put("Сталлоне8", new Date("JUNE 1 1986"));
        map.put("Сталлоне9", new Date("JUNE 1 1987"));

        //Напишите тут ваш код
        return map;
    }

    public static void removeAllSummerPeople(HashMap<String, Date> map)
    {
        //Напишите тут ваш код
//        Iterator it = map.entrySet().iterator();
//        while (it.hasNext())
//        {
//            Map.Entry<String, Date> pair = (Map.Entry<String, Date>) it.next();
//            String date = pair.getValue().toString();
//            if (date.contains("Ju")||date.contains("Aug"))
//            {
//                it.remove();
//            }
//
//        }
        Map<String, Date> mapCopy = new HashMap<String, Date>(map);
        for(Map.Entry<String, Date> pair : mapCopy.entrySet()) {
            String date = pair.getValue().toString();
            if (date.contains("Ju")||date.contains("Aug"))
            {
//                it.remove();
                map.remove(pair.getKey());
            }
        }
    }

    public static void main(String[] args)
    {
        HashMap<String, Date> crud = createMap();
        removeAllSummerPeople(crud);
        for (Map.Entry<String, Date> pair : crud.entrySet())
        {
            String date = pair.getValue().toString();
            String fam = pair.getKey();
            System.out.println(fam + ":" + date);
        }
    }
}

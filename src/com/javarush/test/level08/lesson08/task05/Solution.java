package com.javarush.test.level08.lesson08.task05;

import java.util.*;

/* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

public class Solution
{
    public static HashMap<String, String> createMap()
    {
        //Напишите тут ваш код
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Afsd","Idf");
        map.put("Bfsd","Idertf");
        map.put("Cfsd","Idf");
        map.put("Dfsd","Isdfdf");
        map.put("Efsd","Idf");
        map.put("Ffsd","Ivcbdf");
        map.put("Gfsd","Idf");
        map.put("Hfsd","Ivcbdf");
        map.put("Ifsd","Idf");
        map.put("Tfsd","Idf");
        return map;

    }

    public static void removeTheFirstNameDuplicates(HashMap<String, String> map)
    {
        //Напишите тут ваш код
        HashMap<String, String> map2 = new HashMap<String, String>(map);
        HashMap<String, String> map3 = new HashMap<String, String>(map);

        for (HashMap.Entry<String, String> pair : map2.entrySet())
        {
            map3.remove(pair.getKey());

            if (map3.containsValue(pair.getValue()))
            {
                removeItemFromMapByValue(map, pair.getValue());
            }
        }


    }

    public static void removeItemFromMapByValue(HashMap<String, String> map, String value)
    {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair: copy.entrySet())
        {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }

    public static void main(String[] args){
        HashMap<String, String> test = createMap();
        System.out.println("do");
        for (HashMap.Entry<String, String> pair : test.entrySet()){
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }
        removeTheFirstNameDuplicates(test);
        System.out.println("posle");
        for (HashMap.Entry<String, String> pair : test.entrySet()){
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }
    }
}

package com.javarush.test.level08.lesson08.task03;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/

public class Solution
{
    public static HashMap<String, String> createMap()
    {
        //Напишите тут ваш код
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Ivanov", "Artem");
        map.put("Glazov", "Vitya");
        map.put("Belkina", "Pol");
        map.put("Rolov", "Misha");
        map.put("Barakov", "Ilya");
        map.put("Kolof", "Pol");
        map.put("Polotov", "Pol");
        map.put("Kilova", "Liza");
        map.put("Kpolo", "Liza");
        map.put("Kpoloz", "Pol");

        return map;
    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name)
    {
        //Напишите тут ваш код
        int countName = 0;
        for (Map.Entry<String, String> hashMap:map.entrySet()){
            String mapName = hashMap.getValue();
            if(mapName.equals(name))
                countName++;
        }
        return countName;

    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String familiya)
    {
        //Напишите тут ваш код
        int countFam = 0;
        for (Map.Entry<String, String> hashMap:map.entrySet()){
            String mapName = hashMap.getKey();
            if(mapName.equals(familiya))
                countFam++;
        }
        return countFam;
    }
}

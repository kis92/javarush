package com.javarush.test.level08.lesson08.task02;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/* Удалить все числа больше 10
Создать множество чисел(Set<Integer>), занести туда 20 различных чисел.
Удалить из множества все числа больше 10.
*/

public class Solution
{
    public static HashSet<Integer> createSet()
    {
        //Напишите тут ваш код
        HashSet<Integer> strings = new HashSet<Integer>();
        Random random = new Random();
        for (int i = 0; i < 20; i++)
        {
            strings.add(random.nextInt());
        }
        return strings;
    }

    public static HashSet<Integer> removeAllNumbersMoreThan10(HashSet<Integer> set)
    {
        //Напишите тут ваш код
        HashSet<Integer> integers = new HashSet<Integer>();
        for (Integer i : set){
            if (i < 10)
            {
                integers.add(i);
            }
        }
        return integers;
    }
}

package com.javarush.test.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args)
    {
        //Написать тут ваш код
        ArrayList<Human> test = new ArrayList<Human>();
        Human san =  new Human("san", true, 8, test);
        Human sanLess =  new Human("sanLess", true, 10, test);
        Human daughter =  new Human("daughter", false, 8, test);

        ArrayList<Human> child = new ArrayList<Human>();
        child.add(san);
        child.add(sanLess);
        child.add(daughter);

        Human father =  new Human("father", true, 28, child);
        Human mother =  new Human("mother", false, 28, child);

        ArrayList<Human> pap = new ArrayList<Human>();
        pap.add(father);

        Human grandPa1 = new Human("ded1", true, 58, pap);
        Human grandMa1 = new Human("bab1", false, 58, pap);

        ArrayList<Human> mom = new ArrayList<Human>();
        mom.add(mother);

        Human grandPa2 = new Human("ded2", true, 48, mom);
        Human grandMa2 = new Human("bab2", false, 48, mom);

        System.out.println(san);
        System.out.println(sanLess);
        System.out.println(daughter);
        System.out.println(father);
        System.out.println(mother);
        System.out.println(grandPa1);
        System.out.println(grandMa1);
        System.out.println(grandPa2);
        System.out.println(grandMa2);
    }

    public static class Human
    {
        private String name;
        private boolean sex;
        private int age;
        private ArrayList<Human> children;
        //Написать тут ваш код

        public Human(String name, boolean sex, int age, ArrayList<Human> children)
        {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = children;
        }

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0)
            {
                text += ", дети: "+this.children.get(0).name;

                for (int i = 1; i < childCount; i++)
                {
                    Human child = this.children.get(i);
                    text += ", "+child.name;
                }
            }

            return text;
        }
    }

}

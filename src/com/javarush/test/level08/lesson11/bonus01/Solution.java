package com.javarush.test.level08.lesson11.bonus01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* Номер месяца
Программа вводит с клавиатуры имя месяца и выводит его номер на экран в виде: «May is 5 month».
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        //add your code here - напиши код тут
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String month = reader.readLine();
        int count;

        if (month.equalsIgnoreCase("JANUARY"))
        {
            count = 1;

        } else if (month.equalsIgnoreCase("FEBRUARY"))
        {
            count = 2;

        } else if (month.equalsIgnoreCase("MARCH"))
        {
            count = 3;

        } else if (month.equalsIgnoreCase("APRIL"))
        {
            count = 4;

        } else if (month.equalsIgnoreCase("MAY"))
        {
            count = 5;

        } else if (month.equalsIgnoreCase("JUNE"))
        {
            count = 6;

        } else if (month.equalsIgnoreCase("JULY"))
        {
            count = 7;

        } else if (month.equalsIgnoreCase("AUGUST"))
        {
            count = 8;

        } else if (month.equalsIgnoreCase("SEPTEMBER"))
        {
            count = 9;

        } else if (month.equalsIgnoreCase("OCTOBER"))
        {
            count = 10;

        } else if (month.equalsIgnoreCase("NOVEMBER"))
        {
            count = 11;

        } else if (month.equalsIgnoreCase("DECEMBER"))
        {
            count = 12;

        } else
        {
            count = 0;
        }

        System.out.println(month + " is " + count + " month");
    }

}

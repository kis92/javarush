package com.javarush.test.level16.lesson13.bonus02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* Клубок
1. Создай 5 различных своих нитей c отличным от Thread типом:
1.1. нить 1 должна бесконечно выполняться;
1.2. нить 2 должна выводить "InterruptedException" при возникновении исключения InterruptedException;
1.3. нить 3 должна каждые полсекунды выводить "Ура";
1.4. нить 4 должна реализовать интерфейс Message, при вызове метода showWarning нить должна останавливаться;
1.5. нить 5 должна читать с консоли цифры пока не введено слово "N", а потом вывести в консоль сумму введенных цифр.
2. В статическом блоке добавь свои нити в List<Thread> threads в перечисленном порядке.
3. Нити не должны стартовать автоматически.
Подсказка: Нить 4 можно проверить методом isAlive()
*/

public class Solution {
    public static List<Thread> threads = new ArrayList<Thread>(5);
    static{
        Thread t1 = new ThreadOne();
        Thread t2 = new ThreadTwo();
        Thread t3 = new ThreadThree();
        Thread t4 = new ThreadFour();
        Thread t5 = new ThreadFive();
        threads.add(t1);
        threads.add(t2);
        threads.add(t3);
        threads.add(t4);
        threads.add(t5);
    }

    private static class ThreadOne extends Thread
    {
        @Override
        public void run()
        {
            while(true){
            }
        }
    }

    private static class ThreadTwo extends Thread
    {
        @Override
        public void run()
        {
            try{
                throw new InterruptedException();
            }catch (InterruptedException ex){
                System.out.println("InterruptedException");
            }
        }
    }

    private static class ThreadThree extends Thread
    {
        @Override
        public void run()
        {
            for(;;){
                try
                {
                    this.sleep(500);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                System.out.println("Ура");
            }
        }
    }

    private static class ThreadFour extends Thread implements Message
    {
        public void showWarning(){
            this.interrupt();
            try{
                this.join();
            } catch (Exception e){}
        }
        public void run(){
            while (!isInterrupted()){
            }
        }
    }

    private static class ThreadFive extends Thread
    {
        @Override
        public void run()
        {
            long result = 0;
            BufferedReader reader = null;
            try
            {
                reader = new BufferedReader(new InputStreamReader(System.in));
                while (reader != null)
                {
                    String line =reader.readLine();
                    if (!line.equalsIgnoreCase("n"))
                    {
                        result += Long.parseLong(line);
                    }else{
                        System.out.println(result);
                        break;
                    }
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            finally
            {
             if(reader != null)
                 try
                 {
                     reader.close();
                 }
                 catch (IOException e)
                 {
                     e.printStackTrace();
                 }
            }
        }
    }
}

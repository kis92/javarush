package com.javarush.test.level16.lesson13.bonus01;

import com.javarush.test.level16.lesson13.bonus01.common.*;

/**
 * Created || Updated by Kochubey-I on 07.09.2015 in 17:24.
 */
public class ImageReaderFactory
{
    public static ImageReader getReader(ImageTypes types)
    {
        ImageReader result = null;
        if (types == ImageTypes.BMP)
        {
            result =  new BmpReader();
        } else if (types == ImageTypes.JPG)
        {
            result = new JpgReader();
        } else if (types == ImageTypes.PNG)
        {
            result =  new PngReader();
        } else
        {
            throw new IllegalArgumentException("����������� ��� ��������");
        }
        return result;
    }
}
